package laboratorio2;

import java.lang.Math;

public class Desafio7_apimath {

	public static void main(String[] args) {
		// Desafio 7 API MATH
		
		//Funciones Trigonometricas
		double angulo = 45;
		
		System.out.println(" *Funciones Trigonometricas* ");
		System.out.println("El coseno de 45� es "+ Math.cos(angulo));
		System.out.println("El seno de 45� es "+ Math.sin(angulo));
		System.out.println("La tangente de 45� es "+ Math.tan(angulo));
		
		System.out.println("El arco tangente de 45� es "+ Math.atan(angulo));
		System.out.println("Ea arco tangente del cociente de los argumentos es :"+Math.atan2(90,15));
		
		//Funcion Expoencial y Logaritmica
		
		System.out.println(" ");
		System.out.println(" *Funcion Expoencial y Logaritmica* ");
		System.out.println("exp(e) es " +  Math.exp(1));
	    System.out.println("exp(0) es " +  Math.exp(0));
	    
	    System.out.println("log(e) es " + Math.log(1));
	    System.out.println("log(0) es " + Math.log(0));

	    //Contastes PI y e
	    
	    System.out.println(" ");
	    System.out.println(" *PI y e* ");
	    System.out.println("Math.E es el valor de E: "+Math.E);
	    System.out.println("Math.PI es el valor de PI "+Math.PI);
	    
	    
	}

}
