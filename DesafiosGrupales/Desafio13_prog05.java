package desafio13_prog05;

import java.util.Arrays;
import java.util.Comparator;

import javax.swing.JOptionPane;

public class Desafio13_prog05 {

	public static void main(String[] args) {
		
		// Realice un programa que genere una matriz de 3x3 con n�meros ingresados por el usuario por medio de la consola. 
		//Una vez terminada la carga de los elementos se debe mostrar primero la matriz cargada con los datos iniciales y luego la matriz con los datos ordenados de mayor a menor.
		
		int num;
		
		
		// Declarar e inicializar un array multidimensional: matriz
		int matriz[][] = new int[3][3];
		
		// Ingresar valores en cada espacio de la matriz
		for( int x=0 ; x < matriz.length ; x++ ) {
			for( int y=0 ; y < matriz.length ; y++ ) {
				matriz[x][y] = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un numero en la posici�n "+x+y+": "));
			}
		}
		
		System.out.println(" Matriz desordenada: ");
		MostrarMatriz(matriz);
		OrdenarMatriz(matriz);
		
		System.out.println(" ");
		
		System.out.println(" Matriz Ordenada de Mayor a menor: ");
		MostrarMatriz(matriz);
		
	}
	
	static void MostrarMatriz(int matriz[][]) {
		for( int x=0 ; x < matriz.length ; x++ ) {
			for( int y=0 ; y < matriz.length ; y++ ) {
				System.out.print(matriz[x][y]+" ");
			}
			System.out.println();
		}
	}
	
	static void OrdenarMatriz(int matriz[][]) {
		int [] arreglo = new int[9];
		int x=0, y=0, m=0,l=0;
		// Copio todos los elementos de la matriz al arreglo para ordenarlos y luego repetir el proceso
		
		for(int z=0 ; z < 2 ; z++) 
		{
			
			if(z==0) {
				// For de 0 a 8
				for( int j=0 ; j < arreglo.length ; j++ ) 
				{
					// De 0 a 2
					if( j>=0 && j<3 ) 
					{
						arreglo[j] = matriz[x][j];
						if(j==2) {
							x++;
						}
					}
					// De 3 a 5
					if( j>2 && j<6 ) {
						arreglo[j] = matriz[x][y];
						y++;
						if(j==5) {
							x++;
						}
					}
					// De 6 a 8
					if( j>5 && j<9 ) {
						arreglo[j] = matriz[x][m];
						m++;
					}
				}
			} 
			if(z == 0) {
				Arrays.sort(arreglo);

			}
			
			if(z==1){
				m=0;
				y=0;
				x=0;
				// For de 8 a 0
				for( int v=8 ; v >= 0 ; v-- ) 
				{
					// De 8 a 6
					if( v>5 && v<9  ) 
					{
						matriz[x][l] = arreglo[v];
						l++;
						if(v==6) {
							x++;
						}
					}
					// De 5 a 3
					if( v>2 && v<6 ) {
						matriz[x][y] = arreglo[v];
						y++;
						if(v==3) {
							x++;
						}
					}
					// De 6 a 8
					if( v>=0 && v<3 ) {
						matriz[x][m] = arreglo[v];
						m++;
					}
				}
			}
		}
	}

}
