package Desafios_Lab;

import java.util.Scanner;

public class desafio13_prog03 {

    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        int cantpersonas;

        System.out.println("Empecemos a cargar las personas en la matriz");
        System.out.println("Cuantas personas va a ingresar?");
        cantpersonas = leer.nextInt();



        cantpersonas++;

        String matriz[][] = new String[3][cantpersonas];

        // ponemos titulos a la matriz

        matriz[0][0]="NOMBRE";
        matriz[1][0]="DNI   ";
        matriz[2][0]="EDAD  ";

        //llamamos al metodo para cargar la matriz

        CargarMatriz(matriz);
        System.out.println("Lista de personas!");

        //llamamos  para mostrar una matriz
        MostrarMatriz(matriz);
        OrdenarPersonas(matriz);
        leer.close();
    }


    public static void MostrarMatriz(String matriz[][]) {

        for (int filas=0; filas < matriz.length; filas++) {
            System.out.print("|");

            for (int columnas=0; columnas < matriz[filas].length; columnas++) {
                System.out.print (matriz[filas][columnas]);
                System.out.print("|");
                System.out.print("\t");
            }
            System.out.print("\n");
        }
    }
    //metodo estatico de clase, este metodo carga la matriz de personas

    public static void CargarMatriz (String matriz[][]) {
        //empezamos desde la columna 1 porque la 0 es de los titulos

        int filas=0, columnas=1;
        String nombre;
        Scanner pant = new Scanner(System.in);

        while (columnas < matriz[filas].length) {
            System.out.println("Ingrese nombre de la persona Nº"+columnas+":");
            matriz[filas][columnas] = pant.next();
            nombre = matriz[filas][columnas];
            AutoCompletar(filas, columnas, matriz);
            filas++;
            System.out.println("Ingrese DNI de "+nombre+":");
            matriz[filas][columnas] = pant.next();
            AutoCompletar(filas, columnas, matriz);
            filas++;
            System.out.println("Ingrese la edad de "+nombre+":");
            matriz[filas][columnas] = pant.next();
            AutoCompletar(filas, columnas, matriz);
            filas = 0;
            columnas++;
        }
        pant.close();
    }
    //este metodo rellena strings hasta la longitud 15 en cada valor del arreglo

    public static void AutoCompletar(int fila,int columna,String matriz[][]) {
        String palabra = matriz[fila][columna];
        if(palabra.length() < 15) {
            int iter = 15-palabra.length();
            for (int i = 0; i < iter; i++) {
                palabra=palabra+" ";
            }
            matriz[fila][columna] = palabra;
        }
    }

    //metodo para ordenar los nombres y los datos

    public static void OrdenarPersonas(String matriz[][]) {
        int filasLimite = matriz.length, columnasLimite = matriz[0].length, columnas = 1, fila=0, base=1;
        String aux;
        if (columnasLimite == 2) {
            System.out.println("No es necesario ordenar");
        }
        else {
            //definimos un while, base sera la posicion del "pivot" de comparacion

            while (base != columnasLimite-1) {
                /*si la columna (nuestro pivot movil) llego al final,
                 * base avanza 1 posicion y la columna toma el valor de base
                 */
                if (columnas == columnasLimite) {
                    base++;
                    columnas = base;
                }


                if(matriz[fila][base].compareToIgnoreCase(matriz[fila][columnas]) > 0) {
                    aux = matriz[fila][columnas];
                    matriz[fila][columnas] = matriz[fila][base];
                    matriz[fila][base] = aux;

                    //este while se encarga de cambiar los valores de las filas "dni" y "edad"

                    fila++;
                    while (fila < filasLimite ) {
                        aux = matriz[fila][columnas];
                        matriz[fila][columnas] = matriz[fila][base];
                        matriz[fila][base] = aux;
                        fila++;
                    }
                    fila = 0;
                    columnas++;
                }
                else {
                    columnas++;
                }
            }
            System.out.println(" TABLA DE PERSONAS ORDENADA ");
            MostrarMatriz(matriz);
        }
    }

}
