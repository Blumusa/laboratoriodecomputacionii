import java.util.Random;

public class desafio11 {

	public static void main(String[] args) {
		// Desafio11
		
		CuentaCorriente cta0 = new CuentaCorriente("Anita", 3000);
		CuentaCorriente cta1 = new CuentaCorriente("Pedrito", 2000);
		
		CuentaCorriente.Transferencia(cta0, cta1, 500);
		
		System.out.println(cta0.toString());
		System.out.println(cta1.toString());

	}

}

class CuentaCorriente {
	private String nombreTitular;
	private double saldo;
	private long numeroCuenta;
	
	public CuentaCorriente(String nombreTitular, double saldo) {
		this.nombreTitular = nombreTitular;
		this.saldo = saldo;
		
		Random aleatorio = new Random();
		this.numeroCuenta = Math.abs(aleatorio.nextLong());
		
		
	}
	
	public String ingresarDinero(double dinero) {
		if (dinero>0) {
			this.saldo+=dinero;
			return "Se ha realizado la operacion correctamente";
		} else {
			return "No se puede ingresasr un valor negativo";
		}
	}
	
	public void sacarDinero(double dinero) {
		this.saldo-=dinero;
	}

	public double getSaldo() {
		return saldo;
	}

	@Override
	public String toString() {
		return "CuentaCorriente [nombreTitular=" + nombreTitular + ", saldo=" + saldo + ", numeroCuenta=" + numeroCuenta
				+ "]";
	}
	
	public static void Transferencia(CuentaCorriente ctaSalida, CuentaCorriente ctaEntrada, double dinero) {
		ctaEntrada.saldo+=dinero;
		ctaSalida.saldo-=dinero;
		
	}
	
}



