package TtrabajoIndividialBanco;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Banco {

	public static void main(String[] args) {
		// Trabajo Individual BANCO
		CuentaCorriente cta1 = new CuentaCorriente("Marina", 5300, 284612);
		CuentaCorriente cta2 = new CuentaCorriente("Belen", 1000, 135873);
		
		Set<CuentaCorriente> listadoDeCuentas = new HashSet<CuentaCorriente>();
		System.out.println(listadoDeCuentas.toString());
		
		Operaciones.Transferir(cta1, cta2, 500);
		
		listadoDeCuentas.add(cta1);
		listadoDeCuentas.add(cta2);
		
		try {
			ObjectOutputStream flujodesalida = new ObjectOutputStream(new FileOutputStream("E:\\eclipse workspace\\TrabajoIndividual\\src\\TtrabajoIndividialBanco"));
			flujodesalida.writeObject(listadoDeCuentas);
			flujodesalida.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			ObjectInputStream flujoEntrada = new ObjectInputStream(new FileInputStream("E:\\eclipse workspace\\TrabajoIndividual\\src\\TtrabajoIndividialBanco"));
			
			Set<CuentaCorriente> listadoDeCuentasDeEntrada = (Set<CuentaCorriente>) flujoEntrada.readObject();
			System.out.println("ENTRADA: "+listadoDeCuentasDeEntrada);
			
			flujoEntrada.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		System.out.println(listadoDeCuentas.toString());
	}

}

class CuentaCorriente implements Serializable {
	private String nombreTitular;
	private double saldo;
	private int nroCuenta;
	
	public CuentaCorriente(String nombreTitular, double saldo, int nroCuenta) {
		this.nombreTitular = nombreTitular;
		this.saldo = saldo;
		this.nroCuenta = nroCuenta;
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	public double verSaldo() {
		return saldo;
	}

	public void ingresarDinero(double dinero) {
		this.saldo = this.saldo+dinero;
	}
	
	public void retirarDinero(double dinero) {
		this.saldo = this.saldo-dinero;
	}

	public int getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(int nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	@Override
	public String toString() {
		return "CuentaCorriente [nombreTitular=" + nombreTitular + ", saldo=" + saldo + ", nroCuenta=" + nroCuenta
				+ "]";
	}
	
	
	
	
}

class Operaciones {
	public static void Transferir(CuentaCorriente Ingreso, CuentaCorriente Retiro, double monto) {
		Ingreso.ingresarDinero(monto);
		Retiro.retirarDinero(monto);
	}
}




