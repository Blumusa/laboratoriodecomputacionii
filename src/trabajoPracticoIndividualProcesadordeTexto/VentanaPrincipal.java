package trabajoPracticoIndividualProcesadordeTexto;

import java.awt.event.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.awt.*;
import javax.swing.*;
import javax.swing.text.*;

public class VentanaPrincipal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}

}


class MenuFrame extends JFrame {

	public MenuFrame () {
		setTitle(" Bienvenido al Editor de Texto ");
		
		setBounds (500,300,550,400);
		
		MenuLamina milamina = new MenuLamina();
		
		add(milamina);
		
		setVisible(true);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		addWindowListener(new EventosDeVentana());
	}
	
	private class EventosDeVentana implements WindowListener{

		@Override
		public void windowOpened(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosing(WindowEvent e) {
			if (!MenuLamina.savedornot) {
				int reply = JOptionPane.showConfirmDialog(null, "No ha guardado el texto", "�Guardar texto?", JOptionPane.YES_NO_OPTION);
				if (reply == JOptionPane.YES_OPTION) {
					if (MenuLamina.savedornot) {
						MenuLamina.quicksave();
					} else {
						MenuLamina.ficherosSV();
					}
				}
				else {
					System.exit(0);
				}
				
			}	
		}

		@Override
		public void windowClosed(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowIconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowActivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
}


class MenuLamina extends JPanel {

	public MenuLamina( ) {
		
		setLayout(new BorderLayout());
		
		JPanel laminamenu = new JPanel();
		
		JMenuBar mibarra = new JMenuBar();

		//Elementos del menu----------------
		JMenu archivo = new JMenu("Archivo");
		font = new JMenu("Fuente");
		style = new JMenu("Estilo");
		size = new JMenu("Tama�o");
		//archivo-------------------------------------------
		JMenuItem open = new JMenuItem("Abrir");
		JMenuItem save = new JMenuItem("Guardar");
		archivo.add(open);
		archivo.add(save);
		
		open.addActionListener(new Abriendo());
		save.addActionListener(new Guardando());
		//fuente--------------------------------------------
		config("Arial","Fuente","Arial",9,10);
		config("Courier","Fuente","Courier",9,10);
		config("Verdana","Fuente","Verdana",9,10);
		//Estilo-------------------------------------------
		config("Negrita","Estilo","",Font.BOLD,9);
		config("Cursiva","Estilo","",Font.ITALIC,9);
		//Tama�o-------------------------------------------
		config("8","Tama�o","",1,8);
		config("12","Tama�o","",1,12);
		config("14","Tama�o","",1,14);
		config("16","Tama�o","",1,16);
		config("20","Tama�o","",1,20);
		//--------------------------------------------------
		
		mibarra.add(archivo);
		mibarra.add(font);
		mibarra.add(style);
		mibarra.add(size);
		
		laminamenu.add(mibarra);
		add(laminamenu,BorderLayout.NORTH);
		
		areatexto = new JTextPane();
		
		JScrollPane barrita = new JScrollPane(areatexto);
		
		add(barrita,BorderLayout.CENTER);	
	
	} 
	public void config(String lt, String mu, String tipo_letra, int estilos, int tamano) {
		JMenuItem elem_menu = new JMenuItem(lt);
		if(mu=="Fuente") {
			font.add(elem_menu);
			if (tipo_letra=="Arial") {
				elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("lt", "Arial"));
			} else if (tipo_letra=="Courier") {
				elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("lt", "Courier"));
			} else if (tipo_letra=="Verdana") {
				elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("lt", "Verdana"));
			}
			
		} else if(mu=="Estilo") {
			style.add(elem_menu);			
			if (estilos==Font.BOLD) {
			elem_menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
			elem_menu.addActionListener(new StyledEditorKit.BoldAction());
			}else if (estilos==Font.ITALIC) {
			elem_menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_DOWN_MASK));
			elem_menu.addActionListener(new StyledEditorKit.ItalicAction()); }
			
		} else if (mu=="Tama�o") {
			size.add(elem_menu);
			
			elem_menu.addActionListener(new StyledEditorKit.FontSizeAction("cata",tamano));
		}
		
		
	}
	
	private static String savedRoot = "";
	
	private static String savedTextRoot = "";
	
	public static boolean savedornot = false;
	
	public static void ficherosOP () {
		try {
		JFileChooser abrirarchivo = new JFileChooser();
		abrirarchivo.showOpenDialog(areatexto);
		String archive = abrirarchivo.getSelectedFile().getAbsolutePath();
		String archivoLetras = abrirarchivo.getCurrentDirectory() +"\\"+abrirarchivo.getSelectedFile().getName()+"Texto";
		
		
		ObjectInputStream leefichero = new ObjectInputStream (new FileInputStream(archive));
		ObjectInputStream leerficheroletras = new ObjectInputStream (new FileInputStream(archivoLetras));
			
		areatexto.setText((String) leefichero.readObject());
		areatexto.setStyledDocument((StyledDocument) leerficheroletras.readObject());
		
		leefichero.close();
		leerficheroletras.close();
		savedornot = false;
			
		} 
		 catch (IOException e) {
			 JOptionPane.showConfirmDialog(null, "No tiene el acceso correcto", "DENEGADO", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
			//  posible error de que no se tenga los permisos adecuados para abrir
			e.printStackTrace();
			
		} catch (ClassNotFoundException ex) {
			// TODO Auto-generated catch block
			System.out.println(" Class Not Found! ");
			ex.printStackTrace();
			return;
		}
		
		
	} public static void ficherosSV() {
		try {
			JFileChooser abrirarchivo = new JFileChooser();
			abrirarchivo.showSaveDialog(areatexto);
			
			File archivo = (File) abrirarchivo.getCurrentDirectory();
			String root = archivo+"\\"+abrirarchivo.getSelectedFile().getName();
			String root_text = archivo+"\\"+abrirarchivo.getSelectedFile().getName()+"Texto";
			
			savedRoot = root;
			savedTextRoot = root_text;
			
			ObjectOutputStream writingfile = new ObjectOutputStream(new FileOutputStream(root));
			ObjectOutputStream writingtextfile = new ObjectOutputStream (new FileOutputStream(root_text));
			writingfile.writeObject(areatexto.getText());
			writingtextfile.writeObject(areatexto.getStyledDocument());
			writingfile.close();
			writingtextfile.close();
			
			JOptionPane.showConfirmDialog(null,"Se ha guardado el archivo" , "�Hecho!", JOptionPane.CLOSED_OPTION, JOptionPane.PLAIN_MESSAGE);
			savedornot = true;
		}
		
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			
			JOptionPane.showConfirmDialog(null, "No tiene el acceso correcto", "DENEGADO", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
			//  posible error de que no se tenga los permisos adecuados para guardar
			e.printStackTrace();
	}
		
	}
	
	public static void quicksave () {
		try {
			ObjectOutputStream writingfile = new ObjectOutputStream(new FileOutputStream(savedRoot));
			ObjectOutputStream writingtextfile;
			
				writingtextfile = new ObjectOutputStream (new FileOutputStream(savedTextRoot));
			
			
			writingfile.writeObject(areatexto.getText());
			writingtextfile.writeObject(areatexto.getStyledDocument());
			writingfile.close();
			writingtextfile.close();
			
		
	}  catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}
	
	
	
	private class Guardando implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// boton guardar
			if (savedornot) {
				MenuLamina.quicksave();
			} else {
				MenuLamina.ficherosSV();
			}
		}
		
	}
	
	private class Abriendo implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			// boton abrir
			MenuLamina.ficherosOP();
		}
		
	}
	
		
	
	static JTextPane areatexto;
	
	JMenu font, style, size;
	
	Font letras;
	
	
	
}




