package TrabajoPracticoLaCalculadora;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Calculadora {

	public static void main(String[] args) {
		// Realizar una calculadora con c�digo Java que lleve adelante las operaciones b�sicas
		
		CalcFrame myframe = new CalcFrame();
		
		myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		myframe.setVisible(true);

	}
	
}

class CalcFrame extends JFrame {
	
	public CalcFrame() {
		setTitle ("Calculadora");
		
		setBounds(500,300,450,300);
		
		CalcPanel mypanel = new CalcPanel();
		
		add(mypanel);
}


class CalcPanel extends JPanel {
	
	
	public CalcPanel () {
		
		principio = true;
		
		setLayout(new BorderLayout());
		
		screen1 = new JButton("0");
		
		add(screen1, BorderLayout.NORTH);
		screen1.setEnabled(false);
		
		
		my2frame = new JPanel();
		my2frame.setLayout(new GridLayout(4,4));
		
		//JButton boton1 = new JButton("1");
		//my2frame.add(boton1);
		
		ActionListener insertar = new Addnumber();
		ActionListener orden = new Accionorden();
		
		addButton("7",insertar);
		addButton("8",insertar);
		addButton("9",insertar);
		addButton("/",orden);
		
		addButton("4",insertar);
		addButton("5",insertar);
		addButton("6",insertar);
		addButton("*",orden);
		
		addButton("1",insertar);
		addButton("2",insertar);
		addButton("3",insertar);
		addButton("-",orden);
		
		addButton("0",insertar);
		addButton(",",insertar);
		addButton("+",orden);
		addButton("=",orden);
		
		add(my2frame, BorderLayout.CENTER);
		
		ultimaop="=";
	}
	
	private void addButton (String bot, ActionListener listen) {
		JButton boton = new JButton(bot);
		boton.setBackground(Color.BLACK);
		boton.setForeground(Color.pink);
		boton.addActionListener(listen);
		my2frame.add(boton);
	}
	
	private class Addnumber implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// capturar evento
			
			String entrada = e.getActionCommand();
			if (principio) {
				screen1.setText("");
				principio = false;
			}
			
			screen1.setText(screen1.getText() + entrada);
		}
	
		
	}
	
	private class Accionorden implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// operaciones
			String op = e.getActionCommand();
			
			calcular(Double.parseDouble(screen1.getText()));
			
			ultimaop = op;
			
			 principio = true;
		}
		public void calcular(double x) {
			if (ultimaop.equals("+")) {
				resultado+=x;
			} else if (ultimaop.equals("-")) {
				resultado-=x;
			} else if (ultimaop.equals("*")) {
				resultado*=x;
			} else if (ultimaop.equals("/")) {
				resultado/=x;
			} else if (ultimaop.equals("=")) {
				resultado+=x;
			}
			
			screen1.setText(""+resultado);
		}
	}
	
	private JPanel my2frame;
	private JButton screen1;
	private boolean principio;
	//almacenar operaciones
	private double resultado;
	private String ultimaop;
}

}
