package DesafioIndividual6;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;



public class GraphInterface {

	public static void main(String[] args) {
		
	 MyFrame window = new MyFrame ();

	}

}


class MyFrame extends JFrame {
	
	public MyFrame() {
        
		setSize(500, 170);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocation(50, 50);
        setVisible(true);
        Screen panel = new Screen();
        add(panel);
        setTitle("Desaf�o Individual 6: Programa con interface gr�fica");
        addMouseListener(new Screen());

    }

}

    class Screen extends JPanel implements MouseListener {

        @Override
        public void setBackground(Color bg) {
            super.setBackground(Color.pink);
        }

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            System.out.println("Se ha producido un evento: Mouse ha dado click en el panel");
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {

        }

        class Foco implements FocusListener{

            @Override
            public void focusGained(FocusEvent focusEvent) {
                if (focusEvent.getSource() == cuadrito1) {
                    System.out.println("Frame no.1 ha ganado foco \n");
                }else{
                    System.out.println("Frame no.2 ha ganado foco \n");
                }
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                if (focusEvent.getSource() == cuadrito1) {
                    System.out.println("Frame no.1 ha perdido el foco\n");

                }else{
                    System.out.println("Frame no.2 ha perdido el foco\n");
                }
            }
        }

        public void paintComponent(Graphics g){
           super.paintComponent(g);
            cuadrito1 = new JTextField();
            cuadrito2 = new JTextField();
            cuadrito1.setBounds(120,10,150,30);
            cuadrito2.setBounds(120,80,150,30);

            add(cuadrito1);
            add(cuadrito2);

            tag1 = new JLabel("Frame no.1:");
            tag2 = new JLabel("Frame no.2:");

            tag1.setBounds(30,10,150,20);
            tag2.setBounds(30,80,150,20);

            add(tag1);
            add(tag2);

            Foco foco = new Foco();
            cuadrito1.addFocusListener(foco);
            cuadrito2.addFocusListener(foco);
        }

        private JTextField cuadrito1, cuadrito2;
        private JLabel tag1, tag2;

    }
