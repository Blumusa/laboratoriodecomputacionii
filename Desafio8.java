package laboratorio2;

import java.util.Scanner;

public class Desafio8 {

	public static void main(String[] args) {
		// Pedir numero por consola y calcular raiz

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Por favor, ingrese un numero para conocer su raiz");
		int num = sc.nextInt();
		
		if (num>=0) {
		System.out.println("La raiz cuadrada de "+num+" es "+Math.sqrt(num)); }
		else {
			System.out.println("La raiz cuadrada de un numero negativo no tiene solucion real");
		}
		
		
		
		
	}

}
